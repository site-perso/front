import React from 'react';
import './ProXP.css';
import FontAwesome from 'react-fontawesome';
import {FormattedMessage} from 'react-intl';
import { Grid, Cell } from 'react-foundation';
import cv_fr from "./../translations/cv.fr.json";

let lang = 'fr'
console.log(cv_fr.proxp[lang])

const pro_xp = [
  {
    'id': 'smile',
    'title': 'Lead Developer',
    'employer': 'Smile Open Source Solutions',
    'city': 'Asnières-sur-Seine',
    'from': '12/2016',
    'to': 'pro.current',
    'link': 'https://www.smile.eu'
  },
  {
    'id': 'datawords',
    'title': 'Lead Webmaster',
    'employer': 'Datawords',
    'city': 'Levallois-Perret',
    'from': '01/2014',
    'to': '11/2016',
    'link': 'https://datawords.com/'
  },
  {
    'id': 'kardiogramm',
    'title': 'Développeur PHP (stage)',
    'employer': 'Kardiogramm',
    'city': 'Reims',
    'from': '06/2013',
    'to': '07/2013',
  }
]


export default function({}) {
    return (
        <Grid>
            <Cell medium={10} offsetOnMedium={1} small={12}>
                <h2 className="section-title cell">
                   <FontAwesome name="laptop-code" />&nbsp;<FormattedMessage id="pro.title" />
                </h2>
                {
                    pro_xp.map((experience, i) => (
                        <Grid key={i} className="job">
                            <Cell small={12}>
                                <h3>{experience.title} <small>@{experience.employer}</small></h3>
                            </Cell>
                            <Cell medium={3} small={12}>
                                <p>{experience.city}</p>
                                <time>
                                    {experience.from} - {'pro.current' == experience.to ? <FormattedMessage id="pro.current" /> : experience.to}
                                </time>
                            </Cell>
                            <Cell medium={9} small={12}>
                                <ul>
                                {
                                    cv_fr.proxp[experience.id].map((line) => (
                                        <li>{line}</li>
                                    ))

                                }
                                </ul>
                                {
                                    experience.link ? <a href={experience.link}><FormattedMessage id="pro.visit" /></a>: ''
                                }
                            </Cell>
                        </Grid>
                    ))
                }
            </Cell>
        </Grid>
    )
}