import React from 'react';
import './Header.css';
import FontAwesome from 'react-fontawesome';
import {FormattedMessage} from 'react-intl';
import { Grid, Cell } from 'react-foundation';
import ToggleableHashTag from '../ToggleableHashTag/ToggleableHashTag';

const info = {
  'name': 'Alexandre HUON',
  'position': 'Lead Backend Developer',
  'image': '/img/picture.jpg',
  'age': (new Date()).getFullYear() - 1990,
  'location': 'Hauts-de-Seine',
  'car': false,
  'licence': true,
};

export default function({ name, age, position, image, location, car=false, licence=false }) {
    return (
        <Grid className="header">
            <Cell medium={7} small={12} offsetOnMedium={1}>
                <h1 className="section-title">{info.name}</h1>
                <h2>{info.position}</h2>
                <ul className="no-bullet">
                    <li>
                        <FontAwesome name="map-marker-alt" />&nbsp;{info.location}
                    </li>
                    <li>
                        <FontAwesome name="user-clock" />&nbsp;<FormattedMessage id="header.age" values={{ years: info.age }} />
                    </li>
                    {info.car && (
                        <li>
                            <FontAwesome name="car-side" />&nbsp;<FormattedMessage id="header.car" />
                        </li>
                    )}
                    {info.licence     && (
                        <li>
                            <FontAwesome name="id-badge" />&nbsp;<FormattedMessage id="header.licence" />
                        </li>
                    )}
                </ul>
                <ToggleableHashTag visibleText="BuzzWord1" revealText="trollWord1" />
                <ToggleableHashTag visibleText="BuzzWord2" revealText="trollWord2" />
                <ToggleableHashTag visibleText="BuzzWord3" revealText="trollWord3" />
                <ToggleableHashTag visibleText="BuzzWord4" revealText="trollWord4" />
            </Cell>
            <Cell medium={3} small={12} className="text-center avatar">
                <img src={info.image} className="thumbnail" alt="{name}" title={info.name} />
                <a href="https://connect.symfony.com/profile/__aleks__">
                    <img src="/img/sf4_certif.png" className="badgeSF" alt="SF4" title="SF4" />
                </a>
            </Cell>
        </Grid>
    )
}