import React from 'react';
import './Misc.css';
import FontAwesome from 'react-fontawesome';
import {FormattedMessage} from 'react-intl';
import { Grid, Cell } from 'react-foundation';


const misc = [
    {
        'label': 'misc.leisure',
        'items': [
            'misc.leisure.travelling',
            'misc.leisure.opensource',
            'misc.leisure.coding',
            'misc.leisure.learning',
            'misc.leisure.sports',
            'misc.leisure.films',
            'misc.leisure.videogames'
        ]
    }
];

export default function() {
    return (
        <Grid>
            <Cell medium={10} offsetOnMedium={1} small={12} className="misc">
                <h2 className="section-title cell">
                   <FontAwesome name="snowboarding" />&nbsp;<FormattedMessage id="misc.title" />
                </h2>
                {
                    misc.map((list, i) => (
                        <React.Fragment key={list.label}>
                            <h3><FormattedMessage id={list.label} /></h3>
                            <div className="listing">
                                {
                                    list.items.map((item, j) => (
                                        <FormattedMessage key={ i + '-' + j} id={item} />
                                    ))
                                }
                            </div>
                        </React.Fragment>
                    ))
                }
            </Cell>
        </Grid>
    )
}