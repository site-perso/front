import React from 'react';
import './Education.css';
import FontAwesome from 'react-fontawesome';
import {FormattedMessage} from 'react-intl';
import { Grid, Cell } from 'react-foundation';

const education = [
    {
        "school_name": "Université de Reims Champagne-Ardennes, Reims (51)",
        "diploma": "Master Gestion Multilingue de l'Information",
        "year_obtained": 2013
    },
    {
        "school_name": "Université de Reims Champagne-Ardennes, Reims (51)",
        "diploma": "Licence de Langue, Littérature et Civilisation Anglo-Saxonne",
        "year_obtained": 2011
    },
    {
        "school_name": "Lycée Paul Verlaine, Rethel (08)",
        "diploma": "Baccalauréat série S",
        "year_obtained": 2008
    },
];

export default function() {
    return (
        <Grid>
            <Cell medium={10} offsetOnMedium={1} small={12}>
                <h2 className="section-title cell">
                   <FontAwesome name="graduation-cap" />&nbsp;<FormattedMessage id="education.title" />
                </h2>
                {
                    education.map((degree, i) => (
                        <Grid key={i} className="degree">
                            <Cell small={12}>
                                <h3>{degree.diploma} <small>{degree.year_obtained}</small></h3>
                                <strong>{degree.school_name}</strong>
                            </Cell>
                        </Grid>
                    ))
                }
            </Cell>
        </Grid>
    )
}