import React, { Component } from 'react';
import './SkillListItem.css'

export default class SkillListItem extends Component {
    constructor(props) {
        super(props)
        this.state = { ...props }
    }

    componentWillReceiveProps(nextProps) {
        this.setState(nextProps);
    }

    render() {
        const { skill, showHomeStack } = this.state

        // Todo: throw in some classname here.
        const itemClass = (
            (skill.homeOnly ? 'perso' : '')
            + (skill.isVisible(showHomeStack) ? '' : ' dimmed')
            + " skilllistitem"
        ).trim()

        if ('' === skill.withModal) {
            return (<span className={itemClass}>{skill.label}</span>)
        }

        return (<span className={itemClass} data-modal={skill.withModal}>{skill.label}</span>)
    }
}