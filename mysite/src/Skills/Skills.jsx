import React, { Component } from 'react'
import './Skills.css'
import SkillListItem from './SkillListItem'
import { Grid, Cell, Button } from 'react-foundation';
import FontAwesome from 'react-fontawesome';
import {FormattedMessage} from 'react-intl';

class SkillItem {
    constructor(label, homeOnly=false) {
        this.label     = label
        this.homeOnly  = homeOnly
    }

    isVisible = (homeShowing) => {
        return false === this.homeOnly || true === homeShowing
    }
}

export default class Skills extends Component {
    static stack = [
        {
            "label": "languages",
            'items': [
                new SkillItem('PHP'),
                new SkillItem('Python', true),
                new SkillItem('JavaScript'),
                new SkillItem('SQL'),
                new SkillItem('Bash'),
                new SkillItem('HTML'),
                new SkillItem('CSS'),
            ]
        },
        {
            "label": "backend",
            'items': [
                new SkillItem('Symfony'),
                new SkillItem('Django', true),
                new SkillItem('Flask', true),
                new SkillItem('Code Igniter', true),
            ]
        },
        {
            "label": "frontend",
            'items': [
                new SkillItem('React'),
                new SkillItem('jQuery'),
                new SkillItem('Bootstrap'),
                new SkillItem('Foundation', true),
            ]
        },
        {
            "label": "storage",
            'items': [
                new SkillItem('MySQL'),
                new SkillItem('Elastic Search'),
                new SkillItem('Redis'),
                new SkillItem('Memcache'),
                new SkillItem('Postgres', true),
            ]
        },
        {
            "label": "devopscicd",
            'items': [
                new SkillItem('Docker'),
                new SkillItem('Ansible'),
                new SkillItem('Vagrant'),
                new SkillItem('Virtualbox'),
                new SkillItem('Jenkins'),
                //new SkillItem('Gitlab', true), // Coming soon
            ]
        },
        // Not there yet, but working on it.
        /*{
            "label": "ml",
            'items': [
                new SkillItem('Tensorflow', true),
            ]
        },*/
        {
            "label": "other",
            'items': [
                new SkillItem('Debian'),
                new SkillItem('Red Hat'),
                new SkillItem('AWS', true),
                new SkillItem('RabbitMQ'),
            ]
        }
    ];

    static languages = [
        {
            'label': 'french',
            'flag': 'fr.png',
            'level': 'native',
        },
        {
            'label': 'english',
            'flag': 'uk.png',
            'level': 'fluent',
        },
        {
            'label': 'german',
            'flag': 'de.png',
            'level': 'medium',
        },
        {
            'label': 'dutch',
            'flag': 'nl.png',
            'level': 'readonly',
        }
    ];

    constructor(props) {
        super(props)
        this.state = {
            showHomeStack: true
        }
    }

    switchSkills = () => {
        this.setState({
            showHomeStack: !this.state.showHomeStack
        })
    }

    render() {
        const stack             = Skills.stack
        const languages         = Skills.languages
        const { showHomeStack } = this.state

        return (
            <Grid className="skills">
                <Cell medium={10} offsetOnMedium={1}>
                    <h2 className="section-title cell">
                       <FontAwesome name="certificate" />&nbsp;<FormattedMessage id="skills.title" />
                    </h2>
                </Cell>
                <Cell medium={10}  large={6} offsetOnMedium={1}>
                    <h3 className="section-subtitle cell"><FormattedMessage id="skills.technical" /></h3>
                    <Button className="toggleHomeStack" onClick={this.switchSkills}>
                        <FontAwesome name={showHomeStack ? 'eye-slash' : 'eye' } /> <FormattedMessage id={showHomeStack ? 'homeStackHide' : 'homeStackShow' } />
                    </Button>
                    <dl>
                        {
                            stack.map((item, i) => (
                                <React.Fragment key={item.label}>
                                    <dt><FormattedMessage id={item.label} /></dt>
                                    <dd>{
                                        item.items.map((skill, j) => (
                                            <SkillListItem key={ i + '-' + j} skill={skill} showHomeStack={showHomeStack} />
                                        ))
                                    }</dd>
                                </React.Fragment>
                            ))
                        }
                    </dl>
                </Cell>
                <Cell medium={10} large={4} offsetOnMedium={1}>
                    <h3 className="section-subtitle cell"><FormattedMessage id="skills.linguitics" /></h3>
                    <ul className="languagesList">
                        {
                            languages.map((item, i) => (
                                <li key={i}>
                                    <img src={`/img/${item.flag}`} alt={item.label} title={item.label} />&nbsp;<strong><FormattedMessage id={`language.${item.label}`} /></strong><FormattedMessage id={`language.${item.level}`} />
                                </li>
                            ))
                        }
                    </ul>
                </Cell>
            </Grid>
        )
    }
}