import React, { Component } from 'react';
import Header from './Header/Header';
import ProXP from './ProXP/ProXP';
import Skills from './Skills/Skills';
import Education from './Education/Education';
import Misc from './Misc/Misc';

export default class App extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        <Header />
        <ProXP />
        <Education />
        <Skills />
        <Misc />
      </div>
    )
  }
}

