import React, { Component } from 'react'
import './ToggleableHashTag.css'
import {FormattedMessage} from 'react-intl';

export default class ToggleableHashTag extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visibleText: props.visibleText,
            revealText: props.revealText,
            isRevealed: false
        }
    }

    toggle = () => {
        this.setState({
            isRevealed: !this.state.isRevealed
        })
    }

    render() {
        const { visibleText, revealText, isRevealed } = this.state

        return (
            <span className="toggleableHashTag" onClick={this.toggle}>
                {isRevealed ? <FormattedMessage id={`header.${revealText}`} /> : '#' + visibleText}
            </span>
        )
    }
}