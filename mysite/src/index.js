import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

// https://github.com/formatjs/react-intl
// Set up translation. Indeed this is completely overkill, but I liek to test things.
import {IntlProvider, addLocaleData} from "react-intl";
import locale_fr from 'react-intl/locale-data/fr';
import messages_fr from "./translations/fr.json";

addLocaleData([...locale_fr]);

const messages = {
    'fr': messages_fr
};

ReactDOM.render(
    <IntlProvider locale='fr'  messages={messages['fr']}>
        <App />
    </IntlProvider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
