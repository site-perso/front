FROM node:12.5-alpine

WORKDIR /home/node/app

# COPY mysite/package.json /home/node/app

EXPOSE 3000

CMD [ "npm", "start" ]