.PHONY: image container

image:
	@docker build -tsitefront .

container:
	@docker run --rm -d -v "${PWD}/mysite:/home/node/app" -p "3000:3000" sitefront ; \
	echo "\nApplication is running at http://localhost:3000/\n"

halt:
	@docker container stop `docker ps --filter ancestor=sitefront --format="{{.ID}}"`